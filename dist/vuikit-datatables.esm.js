//
//
//

var script = {
  props: [
    'val' ],
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
/* server only */
, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
  if (typeof shadowMode !== 'boolean') {
    createInjectorSSR = createInjector;
    createInjector = shadowMode;
    shadowMode = false;
  } // Vue.extend constructor export interop.


  var options = typeof script === 'function' ? script.options : script; // render functions

  if (template && template.render) {
    options.render = template.render;
    options.staticRenderFns = template.staticRenderFns;
    options._compiled = true; // functional template

    if (isFunctionalTemplate) {
      options.functional = true;
    }
  } // scopedId


  if (scopeId) {
    options._scopeId = scopeId;
  }

  var hook;

  if (moduleIdentifier) {
    // server build
    hook = function hook(context) {
      // 2.3 injection
      context = context || // cached call
      this.$vnode && this.$vnode.ssrContext || // stateful
      this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
      // 2.2 with runInNewContext: true

      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
      } // inject component styles


      if (style) {
        style.call(this, createInjectorSSR(context));
      } // register component module identifier for async chunk inference


      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier);
      }
    }; // used by ssr in case component is cached and beforeCreate
    // never gets called


    options._ssrRegister = hook;
  } else if (style) {
    hook = shadowMode ? function () {
      style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
    } : function (context) {
      style.call(this, createInjector(context));
    };
  }

  if (hook) {
    if (options.functional) {
      // register for functional component in vue file
      var originalRender = options.render;

      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context);
        return originalRender(h, context);
      };
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate;
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
    }
  }

  return script;
}

var normalizeComponent_1 = normalizeComponent;

var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
  return function (id, style) {
    return addStyle(id, style);
  };
}
var HEAD;
var styles = {};

function addStyle(id, css) {
  var group = isOldIE ? css.media || 'default' : id;
  var style = styles[group] || (styles[group] = {
    ids: new Set(),
    styles: []
  });

  if (!style.ids.has(id)) {
    style.ids.add(id);
    var code = css.source;

    if (css.map) {
      // https://developer.chrome.com/devtools/docs/javascript-debugging
      // this makes source maps inside style tags work properly in Chrome
      code += '\n/*# sourceURL=' + css.map.sources[0] + ' */'; // http://stackoverflow.com/a/26603875

      code += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) + ' */';
    }

    if (!style.element) {
      style.element = document.createElement('style');
      style.element.type = 'text/css';
      if (css.media) { style.element.setAttribute('media', css.media); }

      if (HEAD === undefined) {
        HEAD = document.head || document.getElementsByTagName('head')[0];
      }

      HEAD.appendChild(style.element);
    }

    if ('styleSheet' in style.element) {
      style.styles.push(code);
      style.element.styleSheet.cssText = style.styles.filter(Boolean).join('\n');
    } else {
      var index = style.ids.size - 1;
      var textNode = document.createTextNode(code);
      var nodes = style.element.childNodes;
      if (nodes[index]) { style.element.removeChild(nodes[index]); }
      if (nodes.length) { style.element.insertBefore(textNode, nodes[index]); }else { style.element.appendChild(textNode); }
    }
  }
}

var browser = createInjector;

/* script */
var __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('span',{domProps:{"innerHTML":_vm._s(_vm.$parent.imploded_object(_vm.val))}})};
var __vue_staticRenderFns__ = [];

  /* style */
  var __vue_inject_styles__ = function (inject) {
    if (!inject) { return }
    inject("data-v-b22ffa5a_0", { source: "span[data-v-b22ffa5a]{max-width:20%}", map: undefined, media: undefined });

  };
  /* scoped */
  var __vue_scope_id__ = "data-v-b22ffa5a";
  /* module identifier */
  var __vue_module_identifier__ = undefined;
  /* functional template */
  var __vue_is_functional_template__ = false;
  /* style inject SSR */
  

  
  var arrayImplode = normalizeComponent_1(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    browser,
    undefined
  );

//
//
//

var script$1 = {
  props: [
    'val' ],
  computed: {
    value: function value() {
      return (this.val === true || this.val === 'true') ? 'yes' : 'no';
    },
  },
};

/* script */
var __vue_script__$1 = script$1;

/* template */
var __vue_render__$1 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('span',[_vm._v(_vm._s(_vm.value))])};
var __vue_staticRenderFns__$1 = [];

  /* style */
  var __vue_inject_styles__$1 = function (inject) {
    if (!inject) { return }
    inject("data-v-638a4978_0", { source: "span[data-v-638a4978]{text-transform:capitalize}", map: undefined, media: undefined });

  };
  /* scoped */
  var __vue_scope_id__$1 = "data-v-638a4978";
  /* module identifier */
  var __vue_module_identifier__$1 = undefined;
  /* functional template */
  var __vue_is_functional_template__$1 = false;
  /* style inject SSR */
  

  
  var boolean = normalizeComponent_1(
    { render: __vue_render__$1, staticRenderFns: __vue_staticRenderFns__$1 },
    __vue_inject_styles__$1,
    __vue_script__$1,
    __vue_scope_id__$1,
    __vue_is_functional_template__$1,
    __vue_module_identifier__$1,
    browser,
    undefined
  );

//
//
//

var script$2 = {
  props: [
    'val' ],
};

/* script */
var __vue_script__$2 = script$2;

/* template */
var __vue_render__$2 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('span',[_vm._v(_vm._s(_vm._f("moment")(_vm.val,"from", "now")))])};
var __vue_staticRenderFns__$2 = [];

  /* style */
  var __vue_inject_styles__$2 = undefined;
  /* scoped */
  var __vue_scope_id__$2 = undefined;
  /* module identifier */
  var __vue_module_identifier__$2 = undefined;
  /* functional template */
  var __vue_is_functional_template__$2 = false;
  /* style inject */
  
  /* style inject SSR */
  

  
  var dateDiff = normalizeComponent_1(
    { render: __vue_render__$2, staticRenderFns: __vue_staticRenderFns__$2 },
    __vue_inject_styles__$2,
    __vue_script__$2,
    __vue_scope_id__$2,
    __vue_is_functional_template__$2,
    __vue_module_identifier__$2,
    undefined,
    undefined
  );

//
//
//

var script$3 = {
  props: [
    'val' ],
};

/* script */
var __vue_script__$3 = script$3;

/* template */
var __vue_render__$3 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('span',[_c('img',{attrs:{"src":_vm.val}})])};
var __vue_staticRenderFns__$3 = [];

  /* style */
  var __vue_inject_styles__$3 = function (inject) {
    if (!inject) { return }
    inject("data-v-0ca1f08d_0", { source: "img[data-v-0ca1f08d]{max-height:30px;max-width:30px}", map: undefined, media: undefined });

  };
  /* scoped */
  var __vue_scope_id__$3 = "data-v-0ca1f08d";
  /* module identifier */
  var __vue_module_identifier__$3 = undefined;
  /* functional template */
  var __vue_is_functional_template__$3 = false;
  /* style inject SSR */
  

  
  var imageUrl = normalizeComponent_1(
    { render: __vue_render__$3, staticRenderFns: __vue_staticRenderFns__$3 },
    __vue_inject_styles__$3,
    __vue_script__$3,
    __vue_scope_id__$3,
    __vue_is_functional_template__$3,
    __vue_module_identifier__$3,
    browser,
    undefined
  );

//
//
//

var script$4 = {
  props: [
    'val' ],
};

/* script */
var __vue_script__$4 = script$4;

/* template */
var __vue_render__$4 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('span',{domProps:{"innerHTML":_vm._s(_vm.val)}})};
var __vue_staticRenderFns__$4 = [];

  /* style */
  var __vue_inject_styles__$4 = undefined;
  /* scoped */
  var __vue_scope_id__$4 = undefined;
  /* module identifier */
  var __vue_module_identifier__$4 = undefined;
  /* functional template */
  var __vue_is_functional_template__$4 = false;
  /* style inject */
  
  /* style inject SSR */
  

  
  var normal = normalizeComponent_1(
    { render: __vue_render__$4, staticRenderFns: __vue_staticRenderFns__$4 },
    __vue_inject_styles__$4,
    __vue_script__$4,
    __vue_scope_id__$4,
    __vue_is_functional_template__$4,
    __vue_module_identifier__$4,
    undefined,
    undefined
  );

//
//
//
//
//

var script$5 = {
  props: [
    'val' ],
  computed: {
    /**
     * Converts to a number or displays the string
     *
     * @return Mixed value
     */
    value: function value() {
      return (!Number(this.val)) ? this.val : Number(this.val);
    },
  },
};

/* script */
var __vue_script__$5 = script$5;

/* template */
var __vue_render__$5 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('span',[_vm._v("\n  "+_vm._s(_vm.value)+"\n")])};
var __vue_staticRenderFns__$5 = [];

  /* style */
  var __vue_inject_styles__$5 = undefined;
  /* scoped */
  var __vue_scope_id__$5 = undefined;
  /* module identifier */
  var __vue_module_identifier__$5 = undefined;
  /* functional template */
  var __vue_is_functional_template__$5 = false;
  /* style inject */
  
  /* style inject SSR */
  

  
  var number = normalizeComponent_1(
    { render: __vue_render__$5, staticRenderFns: __vue_staticRenderFns__$5 },
    __vue_inject_styles__$5,
    __vue_script__$5,
    __vue_scope_id__$5,
    __vue_is_functional_template__$5,
    __vue_module_identifier__$5,
    undefined,
    undefined
  );

//
//
//
//
//

var script$6 = {
  props: [
    'val' ],
};

/* script */
var __vue_script__$6 = script$6;

/* template */
var __vue_render__$6 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('span',_vm._l((_vm.val),function(item,index){return _c('span',{key:index,staticClass:"str"},[_vm._v(" "+_vm._s(item))])}),0)};
var __vue_staticRenderFns__$6 = [];

  /* style */
  var __vue_inject_styles__$6 = function (inject) {
    if (!inject) { return }
    inject("data-v-811a5ccc_0", { source: ".str[data-v-811a5ccc]{display:block}", map: undefined, media: undefined });

  };
  /* scoped */
  var __vue_scope_id__$6 = "data-v-811a5ccc";
  /* module identifier */
  var __vue_module_identifier__$6 = undefined;
  /* functional template */
  var __vue_is_functional_template__$6 = false;
  /* style inject SSR */
  

  
  var stringArray = normalizeComponent_1(
    { render: __vue_render__$6, staticRenderFns: __vue_staticRenderFns__$6 },
    __vue_inject_styles__$6,
    __vue_script__$6,
    __vue_scope_id__$6,
    __vue_is_functional_template__$6,
    __vue_module_identifier__$6,
    browser,
    undefined
  );

//
//
//

var script$7 = {
  props: [
    'val',
    'columnData',
    'row'
  ],
  methods: {
    /**
     * calls a function when the value is changed
     * 
     */
    valueChanged: function valueChanged() {
      if (this.columnData && Object.keys(this.columnData).includes('onchange')) {
        this.columnData.onchange(this.row, this.val);
      }
    }
  }
};

/* script */
var __vue_script__$7 = script$7;

/* template */
var __vue_render__$7 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.row[_vm.columnData.field]),expression:"row[columnData.field]"}],staticClass:"uk-input",attrs:{"type":"text"},domProps:{"value":(_vm.row[_vm.columnData.field])},on:{"change":_vm.valueChanged,"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.row, _vm.columnData.field, $event.target.value);}}})};
var __vue_staticRenderFns__$7 = [];

  /* style */
  var __vue_inject_styles__$7 = undefined;
  /* scoped */
  var __vue_scope_id__$7 = undefined;
  /* module identifier */
  var __vue_module_identifier__$7 = undefined;
  /* functional template */
  var __vue_is_functional_template__$7 = false;
  /* style inject */
  
  /* style inject SSR */
  

  
  var textinput = normalizeComponent_1(
    { render: __vue_render__$7, staticRenderFns: __vue_staticRenderFns__$7 },
    __vue_inject_styles__$7,
    __vue_script__$7,
    __vue_scope_id__$7,
    __vue_is_functional_template__$7,
    __vue_module_identifier__$7,
    undefined,
    undefined
  );

//
//
//

var script$8 = {
  props: [
    'val',
    'columnData',
    'row'
  ],
  computed: {
    settings: function settings() {
      return {
        min: (this.columnData.inputsettings && this.columnData.inputsettings.min) ? this.columnData.inputsettings.min : 0,
        max: (this.columnData.inputsettings && this.columnData.inputsettings.max) ? this.columnData.inputsettings.max : null,
        placeholder: (this.columnData.inputsettings && this.columnData.inputsettings.placeholder) ? this.columnData.inputsettings.placeholder : null,
      }
    }
  },
  methods: {
    /**
     * Calls a function when the value is changed
     * 
     */
    valueChanged: function valueChanged() {
      if (this.row[this.columnData.field] < this.settings.min) { this.row[this.columnData.field] = this.settings.min; }
      if (this.settings.max && this.row[this.columnData.field] > this.settings.max) { this.row[this.columnData.field] = this.settings.max; }
      if (this.columnData && Object.keys(this.columnData).includes('onchange')) {
        this.columnData.onchange(this.row, this.val);
      }
    }
  }
};

/* script */
var __vue_script__$8 = script$8;

/* template */
var __vue_render__$8 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.row[_vm.columnData.field]),expression:"row[columnData.field]"}],staticClass:"uk-input",attrs:{"type":"number","placeholder":_vm.settings.placeholder,"max":_vm.settings.max,"min":_vm.settings.min},domProps:{"value":(_vm.row[_vm.columnData.field])},on:{"change":_vm.valueChanged,"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.row, _vm.columnData.field, $event.target.value);}}})};
var __vue_staticRenderFns__$8 = [];

  /* style */
  var __vue_inject_styles__$8 = undefined;
  /* scoped */
  var __vue_scope_id__$8 = undefined;
  /* module identifier */
  var __vue_module_identifier__$8 = undefined;
  /* functional template */
  var __vue_is_functional_template__$8 = false;
  /* style inject */
  
  /* style inject SSR */
  

  
  var numberinput = normalizeComponent_1(
    { render: __vue_render__$8, staticRenderFns: __vue_staticRenderFns__$8 },
    __vue_inject_styles__$8,
    __vue_script__$8,
    __vue_scope_id__$8,
    __vue_is_functional_template__$8,
    __vue_module_identifier__$8,
    undefined,
    undefined
  );

var fields = {
  arrayImplode: arrayImplode,
  imageUrl: imageUrl,
  normal: normal,
  number: number,
  dateDiff: dateDiff,
  boolean: boolean,
  stringArray: stringArray,
  textinput: textinput,
  numberinput: numberinput
};

//
/**
 * Loads all the field types
 *
 * @todo Make sure this isn't getting added to the vue instance
 * every time a datatable is initialised
 */

var comps = {};
Object.keys(fields).forEach(function (key) {
  comps[(key + "-vk-datatable-field")] = fields[key];
});

var script$9 = {
  name: 'vuikit-datatable',
  components: comps,
  props: {
    /**
     * Columns that are available for this table
     *
     * @todo Add more validation but without sacrificing performance
     */
    columns: {
      type: Array,
      required: true,
      /**
       * Validates that there is a field set for every column
       *
       * @param Array columns
       * @return Boolean
       */
      validator: function (columns) {
        var valid = true;
        columns.forEach(function (column) {
          if (!Object.keys(column).includes('field')) { valid = false; }
        });
        return valid;
      },
    },
    /**
     * Stores the table rows
     * @var Array
     */
    rows: {
      type: Array,
      required: true,
    },
    /**
     * Amount of results to display per page
     *
     * @var Number
     */
    per_page: {
      type: Number,
      required: false,
      default: 10,
    },
    /**
     * The actions that are available for each row
     * @var Array
     */
    actions: {
      type: Array,
      required: false,
    },

    /**
     * The default sorting for this table
     * @var string
     */
    defaultSort: {
      type: String,
      required: false,
    },

    /**
     * Manualy show the loading symbol
     * @var Boolean
     */
    loadingPropData: {
      type: Boolean,
      required: false,
      default: false,
    }
  },
  data: function data() {
    return {
      loadingData: true,
      /**
       * Initial items in view
       * @var Array
       */
      in_view: [],
      /**
       * Items in view after filtering
       * @var Array
       */
      in_view_2: [],
      /**
       * Items in view after pagination
       * @var Array
       */
      in_view_3: [],
      /**
       * The current sort value
       * @var String
       */
      current_sort: null,
      /**
       * The current sort direction
       * @var String
       */
      current_sort_direction: 'ASC',
      /**
       * The current page were on
       * @var Int
       */
      page: 1,
      /**
       * Selected items in the list
       */
      selected: [],
      /**
       * Fields that are searchable
       */
      searchFields: [],
      /**
       * The current search query
       * @var String
       */
      search_query: null,
      /**
       * The key value object of sorting options with directions
       * sets change to 1 when getting filtered
       *
       * @var Object
       */
      filter_by: {
        change: 0,
      },
    };
  },
  /**
   * Watchers for sorting, pagination and data updating
   */
  watch: {
    current_sort: function current_sort() {
      this.resort();
    },
    current_sort_direction: function current_sort_direction() {
      this.resort();
    },
    page: function page() {
      this.paginate();
    },
    rows: function rows() {
      this.initMethod();
    },
    in_view: function in_view() {
      this.in_view_2 = this.valueFilterer();
    },
    in_view_2: function in_view_2() {
      this.paginate();
    },
    search_query: function search_query() {
      this.in_view_2 = this.valueFilterer();
    },
    /* eslint-disable func-names */
    'filter_by.change': function () {
      this.in_view_2 = this.valueFilterer();
    },
  },
  computed: {
    /**
     * The total amount of rows in the table
     * @return Int
     */
    total_rows: function total_rows() {
      return this.in_view_2.length;
    },
  },
  mounted: function mounted() {
    // Run the init method to build the table
    this.initMethod();
    if ( this.defaultSort ) { this.current_sort = this.defaultSort; }
  },
  methods: {
    /**
     * Fetch the right field type component
     *
     * @param String type
     * @return String component name
     */
    getComponent: function getComponent(type) {
      var field = (!type) ? 'normal' : type;
      field = field.split('_').join(' ');
      /* eslint-disable arrow-body-style */
      field = field.replace(/(?:^\w|[A-Z]|\b\w)/g, function (letter, index) {
        return index === 0 ? letter.toLowerCase() : letter.toUpperCase();
      }).replace(/\s+/g, '');
      return (field + "-vk-datatable-field");
    },
    /**
     * Table initialisation method
     *
     * @return void()
     */
    initMethod: function initMethod() {
      var this$1 = this;

      var self = this;
      this.in_view = this.rows;
      this.in_view_2 = this.valueFilterer();
      this.in_view_3 = this.in_view_2.slice(0, this.per_page - 1);

      this.searchFields = [];

      // loop over the columns to create the structure
      Object.keys(this.columns).forEach(function (col) {
        if (this$1.columns[col].search) {
          this$1.searchFields.push(this$1.columns[col].field);
        }

        if (this$1.columns[col].filter) {
          if ((!self.filter_by, self.columns[col].field)) {
            this$1.$set(self.filter_by, self.columns[col].field, {
              value_now: '',
              items: self.getDistinctValues(self.columns[col].field), // return the distinct field
            });
          }
        }
      });

      this.loadingData = false;
    },
    /**
     * Resort the results based on the current sort item and the current direction
     * @return void
     */
    resort: function resort() {
      if (this.current_sort) {
        var dir = (this.current_sort_direction === 'ASC') ? 'sort_asc' : 'sort_desc';
        this.in_view = this.in_view.sort(this[dir]);
      }
    },
    /**
     * Sort the given items in ascending order
     *
     * @param Mixed a
     * @param Mixed b
     * @return Int
     */
    sort_asc: function sort_asc(a, b) {
      if (a[this.current_sort] < b[this.current_sort]) { return - 1; }
      if (a[this.current_sort] > b[this.current_sort]) { return 1; }
      return 0;
    },
    /**
     * Sort the given items in descending order
     *
     * @param Mixed a
     * @param Mixed b
     * @return Int
     */
    sort_desc: function sort_desc(a, b) {
      if (a[this.current_sort] > b[this.current_sort]) { return -1; }
      if (a[this.current_sort] < b[this.current_sort]) { return 1; }
      return 0;
    },
    /**
     * Create the pagination for the items that are in view
     */
    paginate: function paginate() {
      var start = 0;
      var end = this.per_page;
      if (this.page !== 1) {
        start = (this.page * this.per_page) - this.per_page - 1;
        end = (start + this.per_page) - 1;
      }
      this.in_view_3 = this.in_view_2.slice(start, end);
    },
    /**
     * Get distinct row values for a certain field type
     *
     * @param String field
     * @return Array paginated items with the current sorting
     */
    getDistinctValues: function getDistinctValues(field) {
      var this$1 = this;

      var items = [];

      Object.keys(this.in_view).forEach(function (item) {
        if (!Array.isArray(this$1.in_view[item][field]) && !items.includes(this$1.in_view[item][field])) {
          items.push(this$1.in_view[item][field]);
        } else if (Array.isArray(this$1.in_view[item][field])) {
          this$1.in_view[item][field].forEach(function (val) {
            if (!items.includes(val)) { items.push(val); }
          });
        }
      });

      return items.sort(function (a, b) {
        if (a < b) { return -1; }
        if (a > b) { return 1; }
        return 0;
      });
    },
    /**
     * Filters the rows in the table based on the users selections and search
     *
     * @returns Array items that get shown
     */
    valueFilterer: function valueFilterer() {
      var self = this;
      self.page = 1;

      var itemsArray = this.in_view;

      Object.keys(this.filter_by).forEach(function (filter) {
        if (filter !== 'change') {
          var vn = self.filter_by[filter].value_now;
          itemsArray = itemsArray.filter(function (row) {
            if (!Array.isArray(row[filter])) {
              return !vn || row[filter] === vn;
            } else {
              return !vn || row[filter].includes(vn);
            }
        });
        }
      });

      itemsArray = itemsArray.filter(function (row) {
        if (!self.search_query) { return true; }
        var exists = false;

        Object.keys(self.searchFields).forEach(function (sr) {
          if (row[self.searchFields[sr]] instanceof Array) {
            exists = self
              .imploded_object(row[self.searchFields[sr]])
              .toLowerCase()
              .includes(self.search_query.toLowerCase())
              ? true
              : exists;
          } else if (
            row[self.searchFields[sr]] !== undefined &&
            isNaN(row[self.searchFields[sr]]) &&
            row[self.searchFields[sr]]
              .toLowerCase()
              .includes(self.search_query.toLowerCase())
          ) {
            exists = true;
          } else if (row[self.searchFields[sr]] === self.search_query) {
            exists = true;
          }
        });
        return exists;
      });
      return itemsArray;
    },
    /**
     * Implodes an object to a comma seperated string
     *
     * @param Object obj
     * @return String comma seperated string
     */
    imploded_object: function imploded_object(obj) {
      return Object.values(obj).join(', ');
    },
  },
};

/* script */
var __vue_script__$9 = script$9;

/* template */
var __vue_render__$9 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"DataTable"},[_c('vk-grid',{staticClass:"uk-child-width-expand@s uk-text-center",attrs:{"gutter":"small"}},[_vm._l((_vm.columns),function(column){return _c('div',{directives:[{name:"show",rawName:"v-show",value:(column.filter),expression:"column.filter"}],key:column.field},[_c('div',{staticClass:"uk-margin"},[(_vm.filter_by[column.field])?_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.filter_by[column.field].value_now),expression:"filter_by[column.field].value_now"}],staticClass:"uk-select",on:{"change":[function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.filter_by[column.field], "value_now", $event.target.multiple ? $$selectedVal : $$selectedVal[0]);},function($event){_vm.filter_by.change = _vm.filter_by.change + 1;}]}},[_c('option',{attrs:{"value":""}},[_vm._v("Filter by "+_vm._s(column.title))]),_vm._v(" "),_vm._l((_vm.filter_by[column.field].items),function(ii){return _c('option',{key:ii,domProps:{"value":ii}},[_vm._v("\n            "+_vm._s((column.type == 'boolean') ? (ii == 'true' ) ? 'Yes ': 'No' : ii)+"\n          ")])})],2):_vm._e()])])}),_vm._v(" "),_c('div',{staticClass:"uk-search uk-search-default"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.search_query),expression:"search_query"}],staticClass:"uk-search-input",attrs:{"type":"search","placeholder":"Search..."},domProps:{"value":(_vm.search_query)},on:{"input":function($event){if($event.target.composing){ return; }_vm.search_query=$event.target.value;}}})])],2),_vm._v(" "),_c('table',{staticClass:"uk-table uk-table-hover uk-table-divider"},[_c('thead',[_c('tr',[_vm._l((_vm.columns.filter(function (col) { return col.hidden !== true; })),function(column){return _c('th',{key:column.field},[_vm._v("\n          "+_vm._s(column.title)+"\n          "),(column.sortable)?_c('vk-icon',{staticClass:"float-right sort_by_btn",class:{'active': column.field == _vm.current_sort},attrs:{"icon":(column.field == _vm.current_sort) ? (_vm.current_sort_direction == 'ASC')
              ? 'arrow-up' : 'arrow-down' :  'arrow-down'},on:{"click":function($event){_vm.current_sort = column.field; _vm.current_sort_direction = (_vm.current_sort_direction === 'ASC') ? 'DESC': 'ASC';}}}):_vm._e(),_vm._v(" "),(column.type && column.type == 'action|checkbox')?_c('input',{staticClass:"uk-checkbox",attrs:{"type":"checkbox"}}):_vm._e()],1)}),_vm._v(" "),(_vm.actions)?_c('th',{staticClass:"uk-preserve-width"}):_vm._e()],2)]),_vm._v(" "),_c('tbody',{staticClass:"tablebody"},[(_vm.loadingData || _vm.loadingPropData)?_c('div',{staticClass:"tabledataLoading"},[_c('vk-spinner',{attrs:{"ratio":"1.5"}})],1):_vm._e(),_vm._v(" "),(_vm.in_view_3.length < 1)?_c('tr',[_c('td',{staticStyle:{"text-align":"center"},attrs:{"colspan":_vm.columns.length}},[_vm._v("No results found")])]):_vm._e(),_vm._v(" "),_vm._l((_vm.in_view_3),function(row){return _c('tr',{key:row.id,on:{"click":function($event){return _vm.$emit('row_click', row)}}},[_vm._l((_vm.columns.filter(function (col) { return col.hidden !== true; })),function(column){return _c('td',{key:column.field},[(!column.type || column.type.substring(0, 6) != 'action')?_c('span',[(column.prefix)?_c('span',[_vm._v(_vm._s(column.prefix))]):_vm._e(),_vm._v(" "),_c(_vm.getComponent(column.type),{tag:"component",attrs:{"val":row[column.field],"columnData":column,"row":row}}),_vm._v(" "),(column.suffix)?_c('span',[_vm._v(_vm._s(column.suffix))]):_vm._e()],1):_vm._e()])}),_vm._v(" "),(_vm.actions)?_c('td',_vm._l((_vm.actions),function(action){return _c('vk-icon',{directives:[{name:"vk-tooltip",rawName:"v-vk-tooltip",value:({ title:action.title, animation: 'fade', duration: 100 }),expression:"{ title:action.title, animation: 'fade', duration: 100 }"}],key:action.event,staticClass:"row_action",attrs:{"icon":action.icon},on:{"click":function($event){$event.stopPropagation();return _vm.$emit('row_' +action.event, row)}}})}),1):_vm._e()],2)})],2)]),_vm._v(" "),(_vm.total_rows > _vm.per_page)?_c('vk-pagination',{attrs:{"page":_vm.page,"per-page":_vm.per_page,"total":_vm.total_rows},on:{"update:page":function($event){_vm.page=$event;}}},[_c('vk-pagination-page-first'),_vm._v(" "),_c('vk-pagination-page-prev',{attrs:{"label":"Previous","expanded":""}}),_vm._v(" "),_c('vk-pagination-pages'),_vm._v(" "),_c('vk-pagination-page-next',{attrs:{"label":"Next","expanded":""}}),_vm._v(" "),_c('vk-pagination-page-last')],1):_vm._e()],1)};
var __vue_staticRenderFns__$9 = [];

  /* style */
  var __vue_inject_styles__$9 = function (inject) {
    if (!inject) { return }
    inject("data-v-0c3de44f_0", { source: ".DataTable .sort_by_btn[data-v-0c3de44f]:hover{color:#8a2be2;cursor:pointer}.DataTable .sort_by_btn .active[data-v-0c3de44f]{color:#8a2be2}.uk-table-hover tbody tr[data-v-0c3de44f]:hover,.uk-table-hover>tr[data-v-0c3de44f]:hover{cursor:pointer;background-color:#403d3d;color:#fff}.row_action[data-v-0c3de44f]{background-color:#fff;color:#000;padding:5px;transition:ease all .3s}.row_action[data-v-0c3de44f]:hover{background-color:#000;color:#fff;cursor:pointer}span.float-right[data-v-0c3de44f]{float:right}td[data-v-0c3de44f]{text-align:left}tbody.tablebody[data-v-0c3de44f]{position:relative}tbody.tablebody .tabledataLoading[data-v-0c3de44f]{position:absolute;width:93%;background-color:rgba(255,255,255,.5);height:64%;display:flex;justify-content:center;align-items:center}", map: undefined, media: undefined });

  };
  /* scoped */
  var __vue_scope_id__$9 = "data-v-0c3de44f";
  /* module identifier */
  var __vue_module_identifier__$9 = undefined;
  /* functional template */
  var __vue_is_functional_template__$9 = false;
  /* style inject SSR */
  

  
  var component = normalizeComponent_1(
    { render: __vue_render__$9, staticRenderFns: __vue_staticRenderFns__$9 },
    __vue_inject_styles__$9,
    __vue_script__$9,
    __vue_scope_id__$9,
    __vue_is_functional_template__$9,
    __vue_module_identifier__$9,
    browser,
    undefined
  );

// Import vue component

// install function executed by Vue.use()
function install(Vue) {
  if (install.installed) { return; }
  install.installed = true;
  Vue.component('vk-datatable', component);

}

// Create module definition for Vue.use()
var plugin = {
  install: install,
};

// To auto-install when vue is found
/* global window global */
var GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

// Inject install function into component - allows component
// to be registered via Vue.use() as well as Vue.component()
component.install = install;

// It's possible to expose named exports when writing components that can
// also be used as directives, etc. - eg. import { RollupDemoDirective } from 'rollup-demo';
// export const RollupDemoDirective = component;

export default component;
