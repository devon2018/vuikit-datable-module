import arrayImplode from './arrayImplode.vue';
import boolean from './boolean.vue';
import dateDiff from './dateDiff.vue';
import imageUrl from './imageUrl.vue';
import normal from './normal.vue';
import number from './number.vue';
import stringArray from './stringArray.vue';
import textinput from './textinput.vue';
import numberinput from './numberinput.vue';

const fields = {
  arrayImplode,
  imageUrl,
  normal,
  number,
  dateDiff,
  boolean,
  stringArray,
  textinput,
  numberinput
}


export default fields;
