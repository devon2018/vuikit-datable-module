# VuiKit DataTable plugin

![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)

 VuiKit DataTable plugin adds loads of custom functionality to uikit datatables

  - Search & filtering
  - Row actions with custom icons and handlers

# New Features!

  - string array field
  - number and textinput fields available
  - Ability to add multiple items to filters for single row
  - added loader for loads of data being processed
  - added default sorting option for table
  - Ability to multi select rows
  - Ability to make columns smaller
  - Improved styling of actions column

# Installation

Install via npm

```sh
$ npm i vuikit-datatable
```

```js
import VuiKitDatatable from 'vuikit-datatable'
Vue.component('vk-datatable', VuiKitDatatable)

-- or --

import VuiKitDatatable from 'vuikit-datatable'
Vue.use(VuiKitDatatable)

```

or download and refrence the vuikit-datatables.min.js file in your html

Whats required to use this module:

* [VueJS]
* [VuiKit]
* [VueMoment]

### Usage

```js
<vk-datatable
    :columns="columns"
    :rows="data"
    :per_page="5"
    :actions="actions"
    v-on:row_click="rowClick"
    v-on:row_delete="rowDelete"
    v-on:row_edit="rowEdit"
    defaultSort="" // the default column to sort by
    loadingPropData="" // boolean to show table loader
    selectableRows="" // multiple rows can be selected
    v-on:rowsSelected="" // callback for when a row is selected
    >
</vk-datatable>
var App = new Vue({
    el: '#app',
    data: function(){
        return {
            // the columns and settings for each
            columns: [
                {
                  title: '', // The title of the column
                  field: '', // the field name in the data array\
                  hidden: true|false , // hide this field
                  search: true|false,  // is field searchable
                  filter: true|false , // is filtererable
                  sortable: true|false,  // field is sortable
                  type: '', // field type, see bellow for options
                  prefix:'', // text before the value in the cell
                  suffix: '' // text after the value in the cell
                  inputsettings: {} // takes values used for number and text input fields,
                  onchange: function (row) {}, // callback for input field value changes
                  smallField: true|false // if true will add uk-table-shrink to the column
                },
            ],
            // the data
            data: [],
            // define actions for row
            actions: [
                {title: 'Edit Post', event: 'edit', icon: 'pencil'}, // creates an row_edit event
                {title: 'Delete Post', event: 'delete', icon: 'trash'}, // creates an row_delete event
            ],
        }
    },
    methods: {
        // handler for RowClick Event
        rowClick: function(row){
        },

        // handler for custom row_delete event
        rowDelete: function(item){

        },

        // handler for custom row_edit event
        rowEdit: function(item){

        }
    }
})
```


### Available types are

| VALUE | Description |
| ------ | ------ |
| '' | Text only field |
| 'array_implode' | Imploded array of values comma seperated |
| 'date_diff' | uses moment differece for humans to return the difference between the given data and today |
| 'number' | numeric field used for sorting |
| 'boolean' | Turns a true or false value into a yes or no |
| 'image_url' | Shows a 30x30px thumbnail |
| 'string_array' | Outputs an array of strings with each value on a new line |
| 'textinput' | Shows a text input and calls a callback on value change. available settings (placeholder) |
| 'numberinput' | Shows a numer input and calls a callback on value change. available settings (placeholder, min, max) |


### Todos

 - Full documentation page
 - Export CSV option
 - Setup changelog
 - Setup contributing data
 - Ability to use multiple calls for data pagination


License
----

MIT


[//]: # ()


   [VueJs]: <https://vuejs.org/>
   [VuiKit]: <https://vuikit.js.org>
   [VueMoment]: <https://github.com/brockpetrie/vue-moment>
